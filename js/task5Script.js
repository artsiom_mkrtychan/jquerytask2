(function ($) {
    $.fn.galleryPlugin = function (options) {
        return this.each(function() {
            
            var settings = $.extend({
                animationSpeed: "600",
                color: "red"
            }, options);

            var imgGallery = $(this);
            var imgArray = imgGallery.children("img");
            var imgArraySize = imgArray.length;
            var currentSlideIndex = 0;
            var nextSlideIndex = currentSlideIndex + 1;
            var prevSlideIndex = imgArraySize - 1;

            if (imgArraySize > 0) {
                imgGallery.html("");

                var slidesWrap =
                    $('<div />', {
                        "class": 'slides-wrap'
                    });
                var refsWrap =
                    $('<div />', {
                        "class": 'refs-wrap'
                    });

                var slides =
                    $('<div />', {
                        "class": 'slides'
                    });

                var leftArrowDef =
                    `<div class="arrow-wrap">
                        <a class="arrow left-arrow"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                    </div>`;

                var rightArrowDef =
                    `<div class="arrow-wrap">
                        <a class="arrow right-arrow"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                    </div>`;


                for (i = 0; i < imgArraySize; i++) {
                    var tempRef =
                        $('<a />', {
                            "class": 'ref',
                            "style": 'color:' + settings.color,
                            "href": i
                        });

                    tempRef.append('<i class="fa fa-circle" aria-hidden="true"></i>');

                    refsWrap.append(tempRef);
                }

                imgArray.each(function (idx, val) {
                    var tempSlide = $(val);

                    tempSlide.addClass('slide');

                    if (idx === currentSlideIndex) {
                        tempSlide.addClass('active');
                    } else {
                        tempSlide.addClass('no-active');
                    }

                    slides.append(tempSlide);
                });

                slidesWrap.append(leftArrowDef);
                slidesWrap.append(slides);
                slidesWrap.append(rightArrowDef);

                imgGallery.append(slidesWrap);
                imgGallery.append(refsWrap);

            }

            var leftArrow = $(".left-arrow");
            leftArrow.on("click", function () {

                hideCurrentSlide();

                movePrev();

                showCurrentSlide();

            });

            var rightArrow = $(".right-arrow");
            rightArrow.on("click", function () {

                hideCurrentSlide();

                moveNext();

                showCurrentSlide();
            });

            var ref = $(".ref");
            ref.on("click", function (event) {
                event.preventDefault();

                hideCurrentSlide();

                currentSlideIndex = parseInt($(this).attr("href"));
                moveTo(currentSlideIndex);

                showCurrentSlide();
            });

            var moveNext = function () {
                prevSlideIndex = currentSlideIndex;
                currentSlideIndex = nextSlideIndex;

                if ((nextSlideIndex + 1) >= imgArraySize) {
                    nextSlideIndex = 0;
                } else {
                    ++nextSlideIndex;
                }
            }

            var movePrev = function () {
                nextSlideIndex = currentSlideIndex;
                currentSlideIndex = prevSlideIndex;

                if ((prevSlideIndex - 1) < 0) {
                    prevSlideIndex = imgArraySize - 1;
                } else {
                    --prevSlideIndex;
                }

            }

            var moveTo = function (currentSlideIndex) {
                if (currentSlideIndex === imgArraySize - 1) {
                    nextSlideIndex = 0;
                } else {
                    nextSlideIndex = currentSlideIndex + 1;
                }

                if (currentSlideIndex === 0) {
                    prevSlideIndex = imgArraySize - 1;
                } else {
                    prevSlideIndex = currentSlideIndex - 1;
                }
            }

            var hideCurrentSlide = function () {
                imgArray.eq(currentSlideIndex).fadeOut(settings.animationSpeed);
                imgArray.eq(currentSlideIndex).addClass("no-active");
                imgArray.eq(currentSlideIndex).removeClass("active");
            }

            var showCurrentSlide = function () {
                imgArray.eq(currentSlideIndex).fadeIn(settings.animationSpeed);
                imgArray.eq(currentSlideIndex).addClass("active");
                imgArray.eq(currentSlideIndex).removeClass("no-active");
            }
        });
        
        
        
    }

})(jQuery);


(function (startFunc) {
    startFunc(window.jQuery, window, document);
}(function ($, window, document) {
    $(function () {
        $(".img-gallery").galleryPlugin({
            
        });
    });
}));
